package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Player_UnitTest {

	@Test
	void testGetNameX() {
		Player x = new Player('x');
		assertEquals('x', x.getName());
	}

	@Test
	void testGetNameO() {
		Player o = new Player('o');
		assertEquals('o', o.getName());
	}

	@Test
	void testWin() throws Exception {
		Player x = new Player('x');
		Player o = new Player('o');
		Table TableWin = new Table(x, o);
		TableWin.setRowCol(0, 0);
		TableWin.setRowCol(1, 0);
		TableWin.setRowCol(2, 0);

		TableWin.setRowCol(0, 1);
		TableWin.setRowCol(1, 1);
		TableWin.setRowCol(2, 1);

		TableWin.setRowCol(0, 2);
		TableWin.setRowCol(1, 2);
		TableWin.setRowCol(2, 2);
		TableWin.checkWin();
		assertEquals(1, TableWin.getCurrent().getWin());

	}

	@Test
	void testDrawX() throws Exception {
		Player x = new Player('x');
		Player o = new Player('o');
		Table TableDraw = new Table(x, o);
		TableDraw.setRowCol(0, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 2);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(0, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 2);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(0, 2);
		TableDraw.checkDraw();
		assertEquals(1, x.getDraw());
	}

	@Test
	void testDrawO() throws Exception {
		Player x = new Player('x');
		Player o = new Player('o');
		Table TableDraw = new Table(x, o);
		TableDraw.setRowCol(0, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 2);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(0, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 2);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(0, 2);
		TableDraw.checkDraw();
		assertEquals(1, o.getDraw());
	}

	@Test
	public void testLose() throws Exception {
		Player x = new Player('x');
		Player o = new Player('o');
		Table table = new Table(x, o);
		table.setRowCol(0, 0);
		table.setRowCol(1, 0);
		table.setRowCol(2, 0);
		table.checkWin();
		table.swicthTurn();
		assertEquals(1, table.getCurrent().getLose());
	}

}
