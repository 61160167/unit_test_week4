package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.text.Format;

import org.junit.jupiter.api.Test;

class Table_UnitTest1 {

	@Test
	void testcheckWinRow() throws Exception {
		Player x1 = new Player('x');
		Player o1 = new Player('o');
		Table tr = new Table(x1, o1);
		tr.setRowCol(0, 0);
		tr.setRowCol(0, 1);
		tr.setRowCol(0, 2);

		tr.setRowCol(1, 0);
		tr.setRowCol(1, 1);
		tr.setRowCol(1, 2);

		tr.setRowCol(2, 0);
		tr.setRowCol(2, 1);
		tr.setRowCol(2, 2);

		assertEquals(true, tr.checkWinRow());
	}

	@Test
	void testcheckWinCol() throws Exception {
		Player x2 = new Player('x');
		Player o2 = new Player('o');
		Table tc = new Table(x2, o2);

		tc.setRowCol(0, 0);
		tc.setRowCol(0, 1);
		tc.setRowCol(0, 2);

		tc.setRowCol(1, 0);
		tc.setRowCol(1, 1);
		tc.setRowCol(1, 2);

		tc.setRowCol(2, 0);
		tc.setRowCol(2, 1);
		tc.setRowCol(2, 2);

		assertEquals(true, tc.checkWinCol());

	}

	@Test
	void testcheckWinCross() throws Exception {
		Player x3 = new Player('x');
		Player o3 = new Player('o');
		Table tcross = new Table(x3, o3);

		tcross.setRowCol(0, 0);
		tcross.setRowCol(1, 1);
		tcross.setRowCol(2, 2);
		tcross.setRowCol(0, 2);
		tcross.setRowCol(2, 0);

		assertEquals(true, tcross.checkWinCross());

	}

	@Test
	void testSwicthTurn() {
		Player x4 = new Player('x');
		Player o4 = new Player('o');
		Table tsp = new Table(x4, o4);
		if (tsp.getCurrent().getName() == 'x') {
			tsp.swicthTurn();
			assertEquals('o', tsp.getCurrent().getName());
		} else if (tsp.getCurrent().getName() == 'o') {
			tsp.swicthTurn();
			assertEquals('x', tsp.getCurrent().getName());
		}
	}

	@Test
	void testDraw() throws Exception {
		Player x5 = new Player('x');
		Player o5 = new Player('o');
		Table TableDraw = new Table(x5, o5);
		TableDraw.setRowCol(0, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 0);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(1, 2);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(0, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 1);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(2, 2);
		TableDraw.swicthTurn();
		TableDraw.setRowCol(0, 2);
		assertEquals(true, TableDraw.checkDraw());
	}
}
