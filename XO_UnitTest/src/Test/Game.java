package Test;

import java.util.Scanner;

public class Game {
	Scanner s = new Scanner(System.in);
	private Table table;
	private Player o;
	private Player x;
	private int row;
	private int col;

	Game() {
		x = new Player('X');
		o = new Player('O');
	}

	public void StartGame() {
		table = new Table(x, o);
	}

	public void showWelcome() {
		System.out.println("Welcome to XO Game");
	}

	public void showTable() {
		for (int i = 0; i < table.getTable().length; i++) {
			for (int j = 0; j < table.getTable()[i].length; j++) {
				System.out.print(table.getTable()[i][j] + " ");
			}
			System.out.println();
		}
	}

	public void showTurn() {
		System.out.println("Turn game is : " + table.getCurrent().getName());
	}

	public void inputRowCol() {
		System.out.print("Enter input Row and Col : ");
		try {
			row = s.nextInt() - 1;
			col = s.nextInt() - 1;
			table.setRowCol(row, col);
		} catch (Exception e) {
			showInputMismatch();
			s.nextLine();
			this.inputRowCol();
		}
	}

	public static void showInputMismatch() {
		System.out.println("Input Mismatch !! Please input number[1-3]");
	}

	public void showDraw() {
		System.out.println("Player x and o is Draw");
	}

	public void showWin() {
		System.out.println("Player " + table.getWinPlayer().getName() + " is WIN");
	}

	public void showBye() {
		System.out.println("Good Bye see you next time !!");
	}

	public void showSumgameX(Player player) {
		System.out.print("Player : " + player.getName() + " : win = " + player.getWin() + " lose = " + player.getLose()
				+ " Draw = " + player.getDraw());
	}

	public void showSumgameO(Player player) {
		System.out.print("Player : " + player.getName() + " : win = " + player.getWin() + " lose = " + player.getLose()
				+ " Draw = " + player.getDraw());
	}

	public boolean inputContinue() {
		System.out.print("Do you want to continue playing? (y/n) : ");
		char result = s.next().charAt(0);
		do {
			if (result != 'y' && result != 'n') {
				System.out.print("Do you want to continue playing? (y/n) : ");
				result = s.next().charAt(0);
			}
			if (result == 'y') {
				return true;
			}
			return false;
		} while (true);
	}

	public void rungame() {
		this.StartGame();
		this.showWelcome();
		do {
			this.showTable();
			this.showTurn();
			this.run();
			this.StartGame();
		} while (this.inputContinue());
		this.showSumgameX(x);
		System.out.println();
		this.showSumgameO(o);
		System.out.println();
		this.showBye();
	}

	public void run() {
		while (true) {
			this.inputRowCol();
			this.showTable();
			if (table.checkWin()) {
				this.showWin();
				break;
			} else if (table.checkDraw()) {
				this.showDraw();
				break;
			}
			table.swicthTurn();
			this.showTurn();
		}
	}
}
